package spring.deep.dive.jpa.entity;



import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import spring.deep.dive.jpa.entity.common.AdditionalInfo;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "books")
@Getter
@Setter
@ToString
public class Book extends AdditionalInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    public Book(String title) {
        this.title = title;
    }

    public Book() {

    }
}