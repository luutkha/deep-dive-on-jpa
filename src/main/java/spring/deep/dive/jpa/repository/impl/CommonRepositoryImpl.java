package spring.deep.dive.jpa.repository.impl;

import spring.deep.dive.jpa.entity.common.AdditionalInfo;
import spring.deep.dive.jpa.repository.CommonRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class CommonRepositoryImpl<T extends AdditionalInfo> implements CommonRepository<T> {
    @PersistenceContext
    private EntityManager em;
    @Override
    public void findByCreatedDate(T entity) {
        entity.getCreatedDate();
//        Table table = Entity.class.getAnnotation(entity.getClass().getName());
        em.createNativeQuery("select * from ",entity.getClass().getName().toLowerCase());
//        em.createQuery("Select * from ", T.);
//        em.(entity);

//        em.find(T,)
    }
}
